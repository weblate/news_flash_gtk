mod popover_tag_row;

use crate::app::{Action, App};
use crate::util::Util;
use glib::clone;
use gtk4::{
    prelude::*, subclass::prelude::*, Button, CompositeTemplate, ListBox, ListBoxRow, Popover, Stack,
    StackTransitionType, Widget,
};
use news_flash::models::{ArticleID, Tag, TagID};
use parking_lot::RwLock;
use popover_tag_row::PopoverTagRow;
use std::collections::HashMap;
use std::sync::Arc;

mod imp {
    use super::*;
    use glib::subclass;

    #[derive(Debug, CompositeTemplate)]
    #[template(resource = "/com/gitlab/newsflash/ui_templates/tag_popover.ui")]
    pub struct TagPopover {
        #[template_child]
        pub main_stack: TemplateChild<Stack>,
        #[template_child]
        pub add_button: TemplateChild<Button>,
        #[template_child]
        pub back_button: TemplateChild<Button>,
        #[template_child]
        pub assigned_tag_list: TemplateChild<ListBox>,
        #[template_child]
        pub unassigned_tags_list: TemplateChild<ListBox>,
        #[template_child]
        pub assigned_tags_list_stack: TemplateChild<Stack>,
        #[template_child]
        pub unassigned_tags_list_stack: TemplateChild<Stack>,

        pub assigned_tags: Arc<RwLock<HashMap<TagID, Tag>>>,
        pub unassigned_tags: Arc<RwLock<HashMap<TagID, Tag>>>,
    }

    impl Default for TagPopover {
        fn default() -> Self {
            Self {
                main_stack: TemplateChild::default(),
                add_button: TemplateChild::default(),
                back_button: TemplateChild::default(),
                assigned_tag_list: TemplateChild::default(),
                unassigned_tags_list: TemplateChild::default(),
                assigned_tags_list_stack: TemplateChild::default(),
                unassigned_tags_list_stack: TemplateChild::default(),

                assigned_tags: Arc::new(RwLock::new(HashMap::new())),
                unassigned_tags: Arc::new(RwLock::new(HashMap::new())),
            }
        }
    }

    #[glib::object_subclass]
    impl ObjectSubclass for TagPopover {
        const NAME: &'static str = "TagPopover";
        type ParentType = Popover;
        type Type = super::TagPopover;

        fn class_init(klass: &mut Self::Class) {
            Self::bind_template(klass);
        }

        fn instance_init(obj: &subclass::InitializingObject<Self>) {
            obj.init_template();
        }
    }

    impl ObjectImpl for TagPopover {}

    impl WidgetImpl for TagPopover {}

    impl PopoverImpl for TagPopover {}
}

glib::wrapper! {
    pub struct TagPopover(ObjectSubclass<imp::TagPopover>)
        @extends Widget, Popover;
}

impl TagPopover {
    pub fn new() -> Self {
        glib::Object::new::<Self>(&[]).unwrap()
    }

    pub fn init(&self, article_id: &ArticleID) {
        let imp = imp::TagPopover::from_instance(self);
        let main_stack = imp.main_stack.get();

        imp.add_button
            .connect_clicked(clone!(@weak main_stack => @default-panic, move |_button| {
                main_stack.set_visible_child_full("unassigned_tags", StackTransitionType::SlideLeft);
            }));

        imp.back_button
            .connect_clicked(clone!(@weak main_stack => @default-panic, move |_button| {
                main_stack.set_visible_child_full("assigned_tags", StackTransitionType::SlideRight);
            }));

        self.connect_closed(clone!(@weak main_stack => @default-panic, move |_button| {
            main_stack.set_visible_child_full("assigned_tags", StackTransitionType::None);
        }));

        let assigned_tag_list = imp.assigned_tag_list.get();
        let unassigned_tags_list_stack = imp.unassigned_tags_list_stack.get();
        let assigned_tags_list_stack = imp.assigned_tags_list_stack.get();
        imp.unassigned_tags_list.connect_row_activated(clone!(
            @strong imp.assigned_tags as assigned_tags,
            @strong imp.unassigned_tags as unassigned_tags,
            @strong article_id,
            @weak main_stack => @default-panic, move |list, row|
        {
            let row = row.downcast_ref::<PopoverTagRow>().expect("Failed to cast PopoverTagRow");
            let tag = unassigned_tags.write().remove(&row.id());

            if let Some(tag) = tag {
                Util::send(Action::TagArticle(article_id.clone(), tag.tag_id.clone()));

                let tag_row = PopoverTagRow::new(&tag, true);
                Self::setup_tag_row(
                    &article_id,
                    &tag_row,
                    &assigned_tags,
                    &unassigned_tags,
                    &assigned_tag_list,
                    list,
                    &assigned_tags_list_stack,
                    &unassigned_tags_list_stack);
                assigned_tag_list.insert(&tag_row, -1);

                assigned_tags.write().insert(tag.tag_id.clone(), tag);

                if assigned_tags.read().is_empty() {
                    assigned_tags_list_stack.set_visible_child_name("empty");
                } else {
                    assigned_tags_list_stack.set_visible_child_name("list");
                }

                if unassigned_tags.read().is_empty() {
                    unassigned_tags_list_stack.set_visible_child_name("empty");
                } else  {
                    unassigned_tags_list_stack.set_visible_child_name("list");
                }

                main_stack.set_visible_child_full("assigned_tags", StackTransitionType::SlideRight);
                list.remove(row);
            }
        }));

        self.update(article_id);
    }

    pub fn update(&self, article_id: &ArticleID) {
        let imp = imp::TagPopover::from_instance(self);
        imp.unassigned_tags.write().clear();
        imp.assigned_tags.write().clear();

        while let Some(row) = &imp.assigned_tag_list.row_at_index(0) {
            imp.assigned_tag_list.remove(row);
        }
        while let Some(row) = &imp.unassigned_tags_list.row_at_index(0) {
            imp.unassigned_tags_list.remove(row);
        }

        if let Some(news_flash) = App::default().news_flash().read().as_ref() {
            if let Ok(tags) = news_flash.get_tags_of_article(article_id) {
                for tag in tags {
                    imp.assigned_tags.write().insert(tag.tag_id.clone(), tag);
                }
            }

            if let Ok(tags) = news_flash.get_tags() {
                let tags = tags
                    .iter()
                    .filter_map(|t| {
                        if imp.assigned_tags.read().contains_key(&t.tag_id) {
                            None
                        } else {
                            Some(t.clone())
                        }
                    })
                    .collect::<Vec<Tag>>();

                for tag in tags {
                    imp.unassigned_tags.write().insert(tag.tag_id.clone(), tag);
                }
            }
        }

        if imp.assigned_tags.read().is_empty() {
            imp.assigned_tags_list_stack.set_visible_child_name("empty");
        } else {
            imp.assigned_tags_list_stack.set_visible_child_name("list");
        }

        if imp.unassigned_tags.read().is_empty() {
            imp.unassigned_tags_list_stack.set_visible_child_name("empty");
        } else {
            imp.unassigned_tags_list_stack.set_visible_child_name("list");
        }

        for unassigned_tag in &(*imp.unassigned_tags.read()) {
            imp.unassigned_tags_list
                .insert(&PopoverTagRow::new(&unassigned_tag.1, false), -1);
        }

        for assigned_tag in &(*imp.assigned_tags.read()) {
            let tag_row = PopoverTagRow::new(&assigned_tag.1, true);
            Self::setup_tag_row(
                &article_id,
                &tag_row,
                &imp.assigned_tags,
                &imp.unassigned_tags,
                &imp.assigned_tag_list,
                &imp.unassigned_tags_list,
                &imp.assigned_tags_list_stack,
                &imp.unassigned_tags_list_stack,
            );
            imp.assigned_tag_list.insert(&tag_row, -1);
        }
    }

    fn setup_tag_row(
        article_id: &ArticleID,
        tag_row: &PopoverTagRow,
        assigned_tags: &Arc<RwLock<HashMap<TagID, Tag>>>,
        unassigned_tags: &Arc<RwLock<HashMap<TagID, Tag>>>,
        assigned_tag_list: &ListBox,
        unassigned_tags_list: &ListBox,
        assigned_tags_list_stack: &Stack,
        unassigned_tags_list_stack: &Stack,
    ) {
        tag_row.connect_local(
            "remove-tag",
            false,
            clone!(
                @strong assigned_tags,
                @strong unassigned_tags,
                @strong article_id,
                @weak assigned_tags_list_stack,
                @weak unassigned_tags_list_stack,
                @weak unassigned_tags_list,
                @weak assigned_tag_list => @default-panic, move |val|
            {
                let tag_id_str = val[1].get::<String>()
                    .expect("The value needs to be of type `String`.");


                let tag_row = val[2].get::<ListBoxRow>()
                    .expect("The value needs to be of type `ListBoxRow`.");

                assigned_tag_list.remove(&tag_row);

                let tag = assigned_tags.write().remove(&TagID::new(&tag_id_str));
                if let Some(tag) = tag {
                    let tag_row = PopoverTagRow::new(&tag, false);
                    unassigned_tags_list.insert(&tag_row, -1);

                    Util::send(Action::UntagArticle(article_id.clone(), tag.tag_id.clone()));

                    unassigned_tags.write().insert(tag.tag_id.clone(), tag);

                    if assigned_tags.read().is_empty() {
                        assigned_tags_list_stack.set_visible_child_name("empty");
                    } else {
                        assigned_tags_list_stack.set_visible_child_name("list");
                    }

                    if unassigned_tags.read().is_empty() {
                        unassigned_tags_list_stack.set_visible_child_name("empty");
                    } else  {
                        unassigned_tags_list_stack.set_visible_child_name("list");
                    }
                }
                None
            }),
        );
    }
}
