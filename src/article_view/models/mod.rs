use serde::{Deserialize, Serialize};

#[derive(Clone, Debug, Serialize, Deserialize)]
pub enum ArticleTheme {
    Default,
    Spring,
    Midnight,
    Parchment,
    Gruvbox,
}

impl ArticleTheme {
    pub fn from_str(string: &str) -> Self {
        match string {
            "theme dark" | "theme default" | "dark" | "default" => Self::Default,
            "spring" | "theme spring" => Self::Spring,
            "midnight" | "theme midnight" => Self::Midnight,
            "parchment" | "theme parchment" => Self::Parchment,
            "gruvbox" | "theme gruvbox" => Self::Gruvbox,

            _ => Self::Default,
        }
    }
    pub fn to_str(&self, prefer_dark_theme: bool) -> &str {
        match self {
            Self::Default => {
                if prefer_dark_theme {
                    "theme dark"
                } else {
                    "theme default"
                }
            }
            Self::Spring => "theme spring",
            Self::Midnight => "theme midnight",
            Self::Parchment => "theme parchment",
            Self::Gruvbox => "theme gruvbox",
        }
    }

    pub fn name(&self) -> &str {
        match self {
            Self::Default => "Default",
            Self::Spring => "Spring",
            Self::Midnight => "Midnight",
            Self::Parchment => "Parchment",
            Self::Gruvbox => "Gruvbox",
        }
    }

    pub fn from_i32(v: i32) -> Self {
        match v {
            0 => Self::Default,
            1 => Self::Spring,
            2 => Self::Midnight,
            3 => Self::Parchment,
            4 => Self::Gruvbox,
            _ => Self::Default,
        }
    }

    pub fn to_i32(&self) -> i32 {
        match self {
            Self::Default => 0,
            Self::Spring => 1,
            Self::Midnight => 2,
            Self::Parchment => 3,
            Self::Gruvbox => 4,
        }
    }
}
